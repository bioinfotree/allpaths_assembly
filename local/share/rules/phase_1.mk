# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# TODO:


# name should be "log", not "logs", because
# CacheGroups.pl greates a "log" dir for its "logs" 
log:
	mkdir -p $@

libraries.csv:
	touch $@

groups.csv:
	touch $@

in_libs.csv: libraries.csv
	printf "library_name,project_name,organism_name,type,paired,frag_size,frag_stddev,insert_size,insert_stddev,read_orientation,genomic_start,genomic_end\n" >$@; \
	for ITEM in $(ADD_LIB); do \
		printf "$$ITEM\n" >>$@; \
	done


in_groups.csv: groups.csv
	printf "group_name,library_name,file_name\n">$@; \
	for ITEM in $(ADD_GROUP); do \
		echo "$$ITEM" | cut -f 1,2,5 -d "," --output-delimiter="," >>$@; \
	done

.PHONY: add_libs
add_libs: in_libs.csv log
	$(call load_modules); \
	CacheLibs.pl \
	CACHE_DIR=$$PWD \
	IN_LIBS_CSV=$< \
	ACTION=Add \
	2>&1 \
	| tee $^2/CacheLibs.pl.$@.log

# must be run after CacheLibs.pl
.PHONY: add_groups
add_groups: in_groups.csv log add_libs
	!threads
	$(call load_modules); \
	CacheGroups.pl \
	CACHE_DIR=$$PWD \
	IN_GROUPS_CSV=$< \
	HOSTS="$$THREADNUM" \   * list of hosts to use in parallel by forking. Each fork converts a single data file *
	ACTION=Add \
	2>&1 \
	| tee $^2/CacheGroups.pl.$@.log


ifdef RM_LIB
rm_libs.csv: libraries.csv
	printf "library_name,project_name,organism_name,type,paired,frag_size,frag_stddev,insert_size,insert_stddev,read_orientation,genomic_start,genomic_end\n" >$@; \
	for ITEM in $(RM_LIB); do \
	printf "$$ITEM\n" >>$@; \
	done

rm_groups.csv: groups.csv
	printf "group_name,library_name,file_name\n">$@; \
	for ITEM in $(RM_GROUP); do \
	printf "$$ITEM\n" >>$@; \
	done

.PHONY: rm_libs
rm_libs: rm_libs.csv log add_libs
	$(call load_modules); \
	CacheLibs.pl \
	CACHE_DIR=$$PWD \
	IN_LIBS_CSV=$< \
	ACTION=Remove \
	2>&1 \
	| tee $^2/CacheLibs.pl.$@.log

# must be run after CacheLibs.pl
.PHONY: rm_groups
rm_groups: rm_groups.csv log rm_libs
	!threads
	$(call load_modules); \
	CacheGroups.pl \
	CACHE_DIR=$$PWD \
	IN_GROUPS_CSV=$< \
	HOSTS="$$THREADNUM" \   * list of hosts to use in parallel by forking. Each fork converts a single data file *
	ACTION=Remove \
	2>&1 \
	| tee $^2/CacheGroups.pl.$@.log
endif




.PHONY: test
test:
	@echo 



ALL +=  log \
	groups.csv \
	libraries.csv \
	in_libs.csv \
	in_groups.csv \
	add_libs \
	add_groups


INTERMEDIATE += rm_groups.csv \
		rm_libs.csv

CLEAN += picard_tmp \
	 qualb_stats \
	 backup \
	 $(wildcard *.fastb) \
	 $(wildcard *.qualb)