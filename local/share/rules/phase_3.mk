# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# TODO:


# force phase_3 to be executed after phase_2
extern ../phase_2/$(FILTER_LENGTH)bp.final.assembly.fasta



help_me.tar.gz:
	$(call load_modules); \
	HelpMe.pl SUBDIR=../phase_2/$(ORGANISM_NAME)/data/run/ASSEMBLIES/$(PRJ_NAME); \
	cp ../phase_2/$(ORGANISM_NAME)/$@ $@

final.summary:
	cp ../phase_2/$(ORGANISM_NAME)/data/run/ASSEMBLIES/$(PRJ_NAME)/$@ $@


# I want all files in the current dir, so
# I use tar with --transform option which change name of extracted files.
# Moreover the --show-transformed, list transformed file name to STDOUT.
# A lot of EMPTY directories remain, but will be deleted during cleaning
extract.ls: help_me.tar.gz
	tar xvzf $< \
	--transform='s:.*/::' \   * extract filenames from full paths *
	--show-transformed \   * show name of files that were extracted *
	>$@


.PHONY: _Makefile
# since one of the extracted file is named Makefile, it interferes
# with makefile of bmake and DURING CLEANING ALL TARGHET OF IT (ASSEMBLY FILES) WILL BE REMOVED!
_Makefile: extract.ls
	if [ -s Makefile ]; then \
	  mv Makefile $@; \
	fi



# plot spectrum of kmer frequencies
plot.spectrum.gp: extract.ls
	$(call load_modules); \
	function join { local IFS="$$1"; shift; echo "$$*"; }; \   * function that join some strings with commas *
	SPECTRA="{$$(join , $$(ls -1 *.kspec))}"; \
	KmerSpectrumPlot.pl \
	SPECTRA=$$SPECTRA \
	GIF=1 \
	HEAD_OUT=$(basename $@) \
	ORGANISM=$(ORGANISM_NAME)

messed.statistics: extract.ls
	paste -s -d \\n \
	<(sed -n '/^RunAllPathsLG/,/^---/p' <command \   * command line *
		| tr -s "\\" " "  \
		| sed '/^[---]/ d' \
		| tr -s "\\n" " "  \
		| sed '1icommand') \
	<(sed 's/^ *//' <mm.report \   * mm.report *
		| sed 's/\([^ ]\) \1/\1_\1/g;s/\([^ ]\) \([^ ]\)/\1_\2/g;s/<<>>//g' \
		| tr -s '[[:blank:]]' \\t \
		| sed '1imm.report' \
		| sed '1i\\') \
	<(sed -n '/^Validating*/,/^Validation*/p' <ValidateAllPathsInputs.out \   * ValidateAllPathsInputs.out *
		| sed 's/[:=]//g' \
		| sed 's/\([^ ]\) \1/\1_\1/g;s/\([^ ]\) \([^ ]\)/\1_\2/g;s/<<>>//g' \
		| tr -s '[[:blank:]]' \\t \
		| sed '/^$$/{N;/^\n$$/d;}'\
		| sed '1iValidateAllPathsInputs.out' \
		| sed '1i\\') \
	<(sed -n '/^Out of*/,/^These marked*/p' <RemoveDodgyReads.out \   * RemoveDodgyReads.out *
		| sed '1iRemoveDodgyReads.out' \
		| sed '1i\\' \
		| tr -d \\t) \
	<(sed -n '/Cheching/,/^$$/p' <RemoveDodgyReads-2.out \   * RemoveDodgyReads-2.out *
		| cut -d ":" -f 4 \
		| sed -e 's/^[ \t]*//' \
		| sed -e 's/\s\+/\t/' \
		| sed '1iRemoveDodgyReads-2.out' \
		| sed '1i\\') \
	<(sed -n '/^Out of*/,/^These marked*/p' <RemoveDodgyReads-2.out \
		| tr -d \\t) \
	<(sed -n '/There/,/^PERFSTAT:*/p' <FillFragments.out \   * FillFragments.out *
		| sed '/^[..]/ d' \
		| sed 's/\([^ ]\) \1/\1_\1/g;s/\([^ ]\) \([^ ]\)/\1_\2/g;s/<<>>//g' \
		| tr -s '[[:blank:]]' \\t \
		| sed '1iFillFragments.out' \
		| sed '1i\\') \
	<(sed -n '/^LEGEND/,/^PERFSTAT/p' <LibCoverage.out \   * LibCoverage.out *
		| sed 's/\([^ ]\) \1/\1_\1/g;s/\([^ ]\) \([^ ]\)/\1_\2/g;s/<<>>//g' \
		| tr -s '[[:blank:]]' \\t \
		| sed '1iLibCoverage.out' \
		| sed '1i\\') \
	<(sed '/^$$/d' <assembly.report \   * assembly.report *
		| sed -n -E '/^-{2,}\sFindErrors/,/^-{2,}/p' \
		| sed -E 's/\s{2,}/\t/g' \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//' \
		| sed '$$ d' \
		| sed '1iassembly.report' \
		| sed '1i\\') \
		<(sed '/^$$/d' <assembly.report \
		| sed -n -E '/^-{2,}\sCleanCorrectedReads/,/^-{2,}\sSamplePairedReadStats/p' \
		| sed -E 's/\s{2,}/\t/g' \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//' \
		| sed '$$ d') \
		<(sed '/^$$/d' <assembly.report \
		| sed -n -E '/^-{2,}\sSamplePairedReadStats/,/^-{2,}\sSamplePairedReadDistributions/p' \
		| sed -E 's/\s{2,}/\t/g' \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//' \
		| sed '$$ d') \
		<(sed '/^$$/d' <assembly.report \
		| sed -n -E '/^-{2,}\sSamplePairedReadDistributions/,/^-{2,}\sLibCoverage/p' \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//' \
		| sed '$$ d' \
		| sed -E 's/\s+\+\/-\s+/\_\+\/\-\_/g' \
		| sed -E 's/\s{2,}/\t/g' \
		| sed -E 's/\se/\te/g' \
		| sed -E 's/> </>\t</g') \
		<(sed '/^$$/d' <assembly.report \
		| sed -n -E '/^-{2,}\sAllPathsReport/,/^$$/p' \
		| sed -E 's/\s{2,}/\t/g' \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//') \
	<(sed -n '/WARNING/,/^File:\s/p' <SamplePairedReadStats.out \   * SamplePairedReadStats.out *
		| sed '1iSamplePairedReadStats.out' \
		| sed '1i\\') \
	<(sed -n -E '/^N50/,/: selected/p' <SamplePairedReadStats.out) \
	<(sed -n -E '/\s+------ MORE DETAILED STATS ------/,/^$$/p' <SamplePairedReadStats.out \
		| sed -E 's/\s+/\t/g' \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//') \
	<(sed -n -E '/\s+------ SHORT STATS ------/,/^$$/p' <SamplePairedReadStats.out \
		| sed -E 's/\s+/\t/g' \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//') \
	<(sed -n '/(SPRD):  /,/(SPRD): /p' <SamplePairedReadDistributions.out \   * SamplePairedReadDistributions.out *
		| sed -E 's/\s+\+\/-\s+/\_\+\/\-\_/g' \
		| sed -E 's/\s{2,}/\t/g' \
		| sed -E 's/\se/\te/g' \
		| sed -E 's/\s!/\t!/g' \
		| cut -f1 --complement \
		| sed -E 's/-{2,}//g' \
		| sed -e 's/^[ \t]*//' \
		| sed '1iSamplePairedReadDistributions.out' \
		| sed '1i\\') \
	<(sed -n '/K_FE/,/END/p' <FindErrors.out \   * FindErrors.out *
		| sed 's/: /\t/g' \
		| cut -f 2 \
		| tr -s '[:blank:]' " " \
		| sed -e 's/^[ \t]*//' \
		| sed 's/CN = 1/CN=1/g;s/Q = /Q=/g' \
		| sed 's/ = /\t/g' \
		| sed '1iFindErrors.out' \
		| sed '1i\\') \
	>$@

# _Makefile.complete: _Makefile
# 	export MAKEFLAGS=""; \
# 	IDILIA_LOADBUILD=yes; \
# 	make -f $< -nprR  RUN=run run/jump_reads_ec.fastb >$@
# 
# _Makefile.dot: _Makefile.complete
# 	make_grapher.py -T $< --sort-dot-entries -o $@
# 
# _Makefile.pdf: _Makefile.dot
# 	circo -Tps $< \
# 	| ps2pdf - >$@

.PHONY: test
test:
	make_grapher.py --help



ALL +=  help_me.tar.gz \
	final.summary \
	_Makefile \
	plot.spectrum.gp \
	messed.statistics


INTERMEDIATE += 

# mextract.ls must be put there, not in ALL
CLEAN += $(wildcard plot.spectrum.*) \
	 extract.ls

# this target will be deleted BEFORE targets in CLEAN
clean: clean_files


.PHONY: clean_files
# use list of extracted file for remove them
clean_files:
	@echo cleaning extracted files...
	if [ -s extract.ls ]; then \
	xargs -I{} sh -c 'rm -rf {}' <extract.ls; \
	fi
