# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# TODO:
#	write an associative array function in python that write to file
#	http://bytes.com/topic/perl/answers/50378-how-read-associative-array-file

MAXPAR					?= 2
FILTER_LENGTH	?= 500
PLOIDY					?= 2
BREAKDOWN		?=

# need for filter_contigs
context prj/abyss_assembly

# get cache dir
extern ../phase_1 as ALLPATHS_RELATIVE_CACHE_DIR



logs:
	mkdir -p $@

.META: in_groups.csv
	1	group_name
	2	library_name
	3	fraction_to_be_used
	4	coverage_to_be_used
	5	file_name

in_groups.csv:
	>$@; \
	for ITEM in $(GROUP) $(ADD_GROUP); do \
		printf "$$ITEM\n" >>$@; \
	done

ref.fasta.gz:
	ln -sf $(REFERENCE) $@


ref.fasta: ref.fasta.gz
	zcat $< >$@

ref.fastb: ref.fasta
	$(call load_modules); \
	Fasta2Fastb IN=$< OUT=$@

ref.len: ref.fasta
	fasta_count -l <$< >$@

# set out the partitioning strategy to be used, fraction or coverage
# if coverage is used, a GENOME_SIZE should also be defined
PART =
ifeq ($(BREAKDOWN),frac)
PART = FRACTIONS="{$$(cut -f3 -d "," $< | paste -s -d ",")}"
endif
ifeq ($(BREAKDOWN),cov)
ifdef REFERENCE
PART = COVERAGES="{$$(cut -f4 -d "," $< | paste -s -d ",")}"
endif
endif


# create input file for the assembly
CacheToAllPathsInputs.flag: in_groups.csv ref.len logs
	$(call load_modules); \
	mkdir -p $(ORGANISM_NAME)/data; \   * DATA_DIR must be created first *
	CacheToAllPathsInputs.pl \
	CACHE_DIR=$$(readlink -f $(ALLPATHS_RELATIVE_CACHE_DIR)) \   * Directory where reads in AllPaths format are cached. Absolute path is needed *
	PLOIDY=$(PLOIDY) \
	GENOME_SIZE=$$(cat <$^2) \
	GROUPS="{$$(cut -f1 -d "," $< | paste -s -d ",")}" \
	$(PART) \
	DATA_DIR=$$PWD/$(ORGANISM_NAME)/data \   * absolute path is needed for DATA_DIR *
	DRY_RUN=0 \
	2>&1 \
	| tee $^3/CacheToAllPathsInputs.pl.$@.log;  \   * [tool].[taget].log *
	touch $@


# run AllPaths
RunAllPathsLG.flag: CacheToAllPathsInputs.flag logs
	!threads
	$(call load_modules); \
	export MAKEFLAGS=""; \   * reset MAKEFLAGS before pass it to RunAllPathsLG which is a makefile *
	RunAllPathsLG \
	PRE=$$PWD \
	REFERENCE_NAME=$(ORGANISM_NAME) \
	DATA_SUBDIR=data \
	RUN=run \
	SUBDIR=$(PRJ_NAME) \
	OVERWRITE=True \
	MAXPAR=$(MAXPAR) \
	THREADS=$$(echo "$$THREADNUM/$(MAXPAR)" | bc) \   * the num of threads should be half of MAXPAR *
	$(call run_allpath_options) \   * optional arguments for Allpath *
	2>&1 \
	| tee $^2/RunAllPathsLG.$@.log; \   * [tool].[taget].log *
	touch $@


# touch dir result dir
$(ORGANISM_NAME): RunAllPathsLG.flag
	touch $@ 


# link final assembly and contigs
final.%.fasta: RunAllPathsLG.flag
	ln -sf ./$(ORGANISM_NAME)/data/run/ASSEMBLIES/$(PRJ_NAME)/final.$*.fasta $@


# filter for scaffolds or contigs length
$(FILTER_LENGTH)bp.final.%.fasta.gz: final.%.fasta
	filter_contigs --fasta $< --length $(FILTER_LENGTH) \
	| gzip -c9 >$@


# get statistics
$(FILTER_LENGTH)bp.final.%.gam: $(FILTER_LENGTH)bp.final.%.fasta.gz
	$(call load_modules); \
	gam-n50 <(zcat $<) \
	| tr \\t \\n >$@


.PHONY: test
test:
	@echo 



ALL +=  logs \
	ref.fasta.gz \
	ref.len \
	in_groups.csv \
	CacheToAllPathsInputs.flag \
	RunAllPathsLG.flag \
	$(ORGANISM_NAME) \
	final.assembly.fasta \
	final.contigs.fasta \
	$(FILTER_LENGTH)bp.final.assembly.fasta.gz \
	$(FILTER_LENGTH)bp.final.contigs.fasta.gz \
	$(FILTER_LENGTH)bp.final.assembly.gam \
	$(FILTER_LENGTH)bp.final.contigs.gam


INTERMEDIATE += 

CLEAN += $(wildcard ref.fast*)
