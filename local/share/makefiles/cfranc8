# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# modules to be loaded for all rules
define load_modules
	module load tools/picard-tools/latest; \
	module load assemblers/allpathslg/48359; \
	module load sw/assemblers/gam-ngs/default
endef

# optional arguments for Allpath
define run_allpath_options
TARGETS=standard \
HAPLOIDIFY=False \
CLOSE_UNIPATH_GAPS=False \   * suggested by scaglione *
DRY_RUN=false
endef
# EVALUATION=BASIC \

# string naming the project. The same for all libraries
PRJ_NAME := cfranc_rejump_228_aplod_false
# string that identifie the organism
ORGANISM_NAME := cabernet_franc
# reference genome
REFERENCE := /mnt/vol1/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta.gz
# sequencing length for filtering contigs and scaffolds
FILTER_LENGTH := 1000
# max parallel targhet to be performed in RunAllPathsLG
# the number of threads specified with '-j flags' will be devided by MAXPAR
MAXPAR := 2
# define the ploidy of the genome
PLOIDY := 2

# initialize empty varibles so I can add values later
LIB :=
GROUP :=

#---------------------------- pired-ends

# math the same field in in_group.csv
LIBRARY_NAME = exp_193_cabernet_franc_Undetermined_L003
# this field in only informative
TYPE = fragment
# needed! 1 paired; 0 unpaired
PAIRED = 1
# only definded for FRAGMENT libraries
FRAG_SIZE = 279
# only definded for FRAGMENT libraries
FRAG_STDEV = 70
# only definded for JUMPING libraries
INSERT_SIZE = 
# only definded for JUMPING libraries
INSERT_STDDEV =
# inward, outward. Outward reads will be reversed
READ_ORIENTATION = inward
# all bases before GENOMIC_START will be trimmed out in each reads
GENOMIC_START = 0
# all bases after GENOMIC_START will be trimmed out in each reads
GENOMIC_END = 0
# UNIQUE name for the dataset
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
# absolute path to the datafile. Wildcards '*' and '?' are accepted but not in the file extension
# supported extensions are: .bam .fasta .fa .fastq .fastq.gz .fq.gz
# if .fasta or .fa are expected .qual .qa
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/paired_end/trimmed/exp_193_cabernet_franc_Undetermined_L003_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#---------------------------- 

LIBRARY_NAME = exp_193_cabernet_franc_Undetermined_L003_unpaired
TYPE = unpaired
PAIRED = 0
FRAG_SIZE =
FRAG_STDEV =
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION =
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/paired_end/trimmed/exp_193_cabernet_franc_Undetermined_L003_unpaired.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)


#----------------------------

LIBRARY_NAME = exp_228_cabernet-franc_GCCAAT_L1_clean_pair
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 400
FRAG_STDEV = 90
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_pair_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_397_cabernet-franc_GCCAAT_L1_clean_pair
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 400
FRAG_STDEV = 90
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_pair_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_397_cabernet-franc_GCCAAT_L2_clean_pair
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 400
FRAG_STDEV = 90
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_pair_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#---------------------------- miseq

LIBRARY_NAME = exp_40_cabernet-franc_TCGCCTTA_M01598
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 400
FRAG_STDEV = 90
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_40_cabernet-franc_TCGCCTTA_M01598_filtered_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_40_cabernet-franc_TCGCCTTA_M01598_merged_unpaired
TYPE = unpaired
PAIRED = 0
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = 
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_40_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_43_cabernet-franc_TCGCCTTA_M01598
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 400
FRAG_STDEV = 90
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward 
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_43_cabernet-franc_TCGCCTTA_M01598_filtered_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_43_cabernet-franc_TCGCCTTA_M01598_merged_unpaired
TYPE = unpaired
PAIRED = 0
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = 
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_43_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_51_cabernet-franc_TCGCCTTA_M01598
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 400
FRAG_STDEV = 90
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_51_cabernet-franc_TCGCCTTA_M01598_filtered_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_51_cabernet-franc_TCGCCTTA_M01598_merged_unpaired
TYPE = unpaired
PAIRED = 0
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = 
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_51_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_54_cabernet-franc_TCGCCTTA_M01598
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 400
FRAG_STDEV = 90
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_54_cabernet-franc_TCGCCTTA_M01598_filtered_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_54_cabernet-franc_TCGCCTTA_M01598_merged_unpaired
TYPE = unpaired
PAIRED = 0
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = 
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_54_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_66_cabernet-franc_TCGCCTTA_M01598
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 469
FRAG_STDEV = 68
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_66_cabernet-franc_TCGCCTTA_M01598_filtered_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_66_cabernet-franc_TCGCCTTA_M01598_merged_unpaired
TYPE = unpaired
PAIRED = 0
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 
INSERT_STDDEV = 
READ_ORIENTATION = 
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_66_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#---------------------------- jumping

LIBRARY_NAME = exp_228_cabernet-franc_GCCAAT_L1_clean_mate
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2562
INSERT_STDDEV = 1115
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_mate_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2562
INSERT_STDDEV = 1115
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)



#----------------------------

LIBRARY_NAME = exp_397_cabernet-franc_GCCAAT_L1_clean_mate
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2562
INSERT_STDDEV = 1115
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_mate_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2562
INSERT_STDDEV = 1115
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_397_cabernet-franc_GCCAAT_L2_clean_mate
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2562
INSERT_STDDEV = 1115
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_mate_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2562
INSERT_STDDEV = 1115
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)
