# Copyright Michele Vidotto 2013 <michele.vidotto@gmail.com>

# modules to be loaded for all rules
define load_modules
	module load tools/picard-tools/latest; \
	module load assemblers/allpathslg/48359; \
	module load sw/assemblers/gam-ngs/default
endef

# optional arguments for Allpath
define run_allpath_options
TARGETS=standard \
HAPLOIDIFY=True \
CLOSE_UNIPATH_GAPS=False \   * suggested by scaglione *
DRY_RUN=false
endef
# EVALUATION=BASIC \

# string naming the project. The same for all libraries
PRJ_NAME := mzebra_test
# string that identifie the organism
ORGANISM_NAME := malawi_zebra
# reference genome
# the reference genome correspond to the assembly composed by scaffolds performed by ALLP group,
# mentioned in the Additional_file_4 (/projects/novabreed/share/mvidotto/doc/novabreed/genome_assembly/assemblathon2_mzebra/)
# downloaded from: ftp://climb.genomics.cn/pub/10.5524/100001_101000/100060/fish_6C_scaffolds.fa.gz
REFERENCE := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra_genome/fish_6C_scaffolds.fa.gz
# sequencing length for filtering contigs and scaffolds
FILTER_LENGTH := 1000
# max parallel targhet to be performed in RunAllPathsLG
# the number of threads specified with '-j flags' will be devided by MAXPAR
MAXPAR := 2
# define the ploidy of the genome
PLOIDY := 2

# initialize empty varibles so I can add values later
LIB :=
GROUP :=

#---------------------------- fragment

# math the same field in in_group.csv
# the library name is taken from Additional_file_9: [Library].[Lane] because the corrispondence in
# Additional_file_3, pag. 6 are wrong
LIBRARY_NAME = Solexa-37938.1
# this field in only informative
TYPE = fragment
# needed! 1 paired; 0 unpaired
PAIRED = 1
# only definded for FRAGMENT libraries
FRAG_SIZE = 180
# only definded for FRAGMENT libraries
FRAG_STDEV = 15
# only definded for JUMPING libraries
INSERT_SIZE = 
# only definded for JUMPING libraries
INSERT_STDDEV =
# inward, outward. Outward reads will be reversed
READ_ORIENTATION = inward
# all bases before GENOMIC_START will be trimmed out in each reads
GENOMIC_START = 0
# all bases after GENOMIC_START will be trimmed out in each reads
GENOMIC_END = 0
# UNIQUE name for the dataset
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
# absolute path to the datafile. Wildcards '*' and '?' are accepted but not in the file extension
# supported extensions are: .bam .fasta .fa .fastq .fastq.gz .fq.gz
# if .fasta or .fa are expected .qual .qa
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077292_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-37938.2
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 180
FRAG_STDEV = 15
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077290_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-37938.3
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 180
FRAG_STDEV = 15
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077286_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-37938.4
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 180
FRAG_STDEV = 15
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077298_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-37938.5
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 180
FRAG_STDEV = 15
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077287_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-37938.6
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 180
FRAG_STDEV = 15
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077302_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-37938.7
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 180
FRAG_STDEV = 15
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077301_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-37938.8
TYPE = fragment
PAIRED = 1
FRAG_SIZE = 180
FRAG_STDEV = 15
INSERT_SIZE =
INSERT_STDDEV =
READ_ORIENTATION = inward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077300_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#---------------------------- jumping

LIBRARY_NAME = Solexa-39016.4
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2500
INSERT_STDDEV = 250
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077295_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-39015.2
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2500
INSERT_STDDEV = 250
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077291_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-39015.3
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 2500
INSERT_STDDEV = 250
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077294_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-50374.8
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 11000
INSERT_STDDEV = 1100
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR099406_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-50806.2
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 9000
INSERT_STDDEV = 900
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR099405_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-50818.1
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 7000
INSERT_STDDEV = 700
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR099407_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-50841.6
TYPE = jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 5000
INSERT_STDDEV = 500
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR099408_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#---------------------------- long_jumping

LIBRARY_NAME = Solexa-46074.1
TYPE = long_jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 40000
INSERT_STDDEV = 4000
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077285_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------

LIBRARY_NAME = Solexa-46074.2
TYPE = long_jumping
PAIRED = 1
FRAG_SIZE = 
FRAG_STDEV = 
INSERT_SIZE = 40000
INSERT_STDDEV = 4000
READ_ORIENTATION = outward
GENOMIC_START = 0
GENOMIC_END = 0
GROUP_NAME = $(PRJ_NAME)_$(LIBRARY_NAME)
FILE_NAME = /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/mzebra_test/mzebra/SRR077289_?.fastq.gz

LIB += $(LIBRARY_NAME),$(PRJ_NAME),$(ORGANISM_NAME),$(TYPE),$(PAIRED),$(FRAG_SIZE),$(FRAG_STDEV),$(INSERT_SIZE),$(INSERT_STDDEV),$(READ_ORIENTATION),$(GENOMIC_START),$(GENOMIC_END)
GROUP += $(GROUP_NAME),$(LIBRARY_NAME),$(FILE_NAME)

#----------------------------